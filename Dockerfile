FROM docker.io/library/ubuntu:22.04

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 \
    TZ=Asia/Shanghai \
    TERM=xterm-256color \
    MAMBA_EXE=/usr/bin/micromamba \
    MAMBA_ROOT_PREFIX=/opt/micromamba \
    PYTHON_VERSION=3.11

RUN APT_INSTALL="apt-get install -y --no-install-recommends --no-install-suggests" && \
    GIT_CLONE="git clone --depth 10" && \
    rm -rf /etc/apt/sources.list.d/cuda.list \
           /etc/apt/sources.list.d/nvidia-ml.list && \
    rm -f /etc/apt/apt.conf.d/docker-clean && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive $APT_INSTALL tzdata ca-certificates git curl bzip2 && \
    ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime && \
    echo ${TZ} > /etc/timezone && \
    dpkg-reconfigure --frontend noninteractive tzdata && \
    # finally clean the cache
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj --strip-components=1 -C /usr/bin bin/micromamba && \
    micromamba shell init -s bash -p ${MAMBA_ROOT_PREFIX} && \
    micromamba create -n dev python=${PYTHON_VERSION} -c conda-forge && \
    echo "micromamba activate dev" >> $HOME/.bashrc

